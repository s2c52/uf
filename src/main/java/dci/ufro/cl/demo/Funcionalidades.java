package dci.ufro.cl.demo;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class Funcionalidades {

    // Se declaran las variables
    public String URL = "https://si3.bcentral.cl/indicadoressiete/secure/Serie.aspx?gcode=UF&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAYwBsAEMAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA=";
    public ArrayList<String> valorUF = new ArrayList<String>();
    private String texto;
    public int mes;
    public String dia;
    public Scanner gl = new Scanner(System.in);

    //Metodo Obtención de textos para guardar
    public String ObtenerTexto(String texto) {
        this.texto = texto;
        return texto;
    }

    //Metodo de Consulta de datos
    public void QueryValor() {
//        Borramos los datos del arreglo
        valorUF.clear();

        try {
            Document doc = Jsoup.connect(URL).get();
//            Se recorre el arreglo que tenga el tag "td", y se agrega a un arreglo <ValorUF>
            for (Element input : doc.getElementsByTag("td")) {
                valorUF.add(ObtenerTexto(input.text()));
            }

//          Se piden los datos al usuario y se rescatan con un Scanner
            System.out.println("Ingrese día del mes ");
            String dia = gl.nextLine();


//           Se validan los parámetros del día y del mes
            if(Integer.parseInt(dia) >= 1 && Integer.parseInt(dia)<=31) {
                System.out.println("Ingrese número del mes");
                mes = gl.nextInt();
                if(mes >= 1 && mes<=12) {
//                    Se consulta por cada columna del arreglo por el día y mes, recorre hasta encontrar el número de la columna (dia), y
//                      se recorre la columna por cada fila del número ingresado(mes).
                    for (int i = 1; i < valorUF.size(); i++) {
                        if (valorUF.get(i).equals(dia)) {
                            System.out.println(valorUF.get(i + mes));
                        }
                    }
                }else{
                    System.out.println("Ha ingresado un valor inválido, intente nuevamente.");
                    System.exit(0);
                }
            }else{
                System.out.println("Ha ingresado un valor inválido, intente nuevamente.");
                System.exit(0);
            }

            // Cambios aplicados a newf1.
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    //  Método de mostrar Tabla
    public void mostrarTabla(){

        try {
            Document doc = Jsoup.connect(URL).get();
            for (Element row : doc.select(
                    "table#gr.table tr")) {

//              Se declaran las columnas totales. Contiene meses y datos de la tabla html.

                final String enero = row.select("th:nth-of-type(2)").text();
                final String febrero = row.select("th:nth-of-type(3)").text();
                final String marzo = row.select("th:nth-of-type(4)").text();
                final String abril = row.select("th:nth-of-type(5)").text();
                final String mayo = row.select("th:nth-of-type(6)").text();
                final String junio = row.select("th:nth-of-type(7)").text();
                final String julio = row.select("th:nth-of-type(8)").text();
                final String agosto = row.select("th:nth-of-type(9)").text();
                final String septiembre = row.select("th:nth-of-type(10)").text();
                final String octubre = row.select("th:nth-of-type(11)").text();
                final String noviembre = row.select("th:nth-of-type(12)").text();
                final String diciembre = row.select("th:nth-of-type(13)").text();

                final String datosEnero = row.select("td:nth-of-type(2)").text();
                final String datosFebrero = row.select("td:nth-of-type(3)").text();
                final String datosMarzo = row.select("td:nth-of-type(4)").text();
                final String datosAbril = row.select("td:nth-of-type(5)").text();
                final String datosMayo = row.select("td:nth-of-type(6)").text();
                final String datosJunio = row.select("td:nth-of-type(7)").text();
                final String datosJulio = row.select("td:nth-of-type(8)").text();
                final String datosAgosto = row.select("td:nth-of-type(9)").text();
                final String datosSeptiembre = row.select("td:nth-of-type(10)").text();
                final String datosOctubre = row.select("td:nth-of-type(11)").text();
                final String datosNoviembre = row.select("td:nth-of-type(12)").text();
                final String datosDiciembre = row.select("td:nth-of-type(13)").text();


//              Se imprimen los datos en forma de tabla
                System.out.println(enero + "\t\t" + febrero + "\t\t" + marzo + "\t\t" + abril + "\t\t" + mayo + "\t\t" + junio + "\t\t"
                        + julio + "\t\t" + agosto + "\t\t" + septiembre + "\t" + octubre + "\t\t" + noviembre + "\t" + diciembre);

                System.out.print(datosEnero + "\t" + datosFebrero + "\t" + datosMarzo + "\t" + datosAbril + "\t" + datosMayo + "\t" + datosJunio + "\t" + datosJulio + "\t" + datosAgosto + "\t" + datosSeptiembre + "\t" + datosOctubre + "\t" + datosNoviembre + "\t" + datosDiciembre + "");
            }
        } catch(Exception e){
            System.out.println(e.getMessage());
        }
    }


}

